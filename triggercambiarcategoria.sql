CREATE TRIGGER cambiarcategoria
ON Post
FOR UPDATE,INSERT
AS
BEGIN
	SET NOCOUNT ON;
	declare @idnuvcatego int 
	set @idnuvcatego=1+(Select MAX (idcategoria) from categorias)
	declare @nomcatego varchar(250) 
	set @nomcatego='nuevacatego'
	set @idnuvcatego=1+(Select MAX (idcategoria) from categorias)

	declare @idpos int 
	select @idpos=idpost from inserted


	 INSERT INTO categorias (idcategoria, nomcategoria) VALUES (@idnuvcatego, @nomcatego);
	
	update Post 
	set idcategoria=@idnuvcatego
	where idpost=@idpos

	
END