use trabajofinal

create table usuarios(
iduser int primary key IDENTITY,
nomuser varchar(250),
);

create table categorias(
idcategoria int primary key IDENTITY,
nomcategoria varchar(250),
);

create table Post(
idpost int primary key IDENTITY,
iduser int,
FOREIGN KEY (iduser) REFERENCES usuarios(iduser),
fecha datetimeoffset,
titulo varchar(2500),
contenido varchar(2500),
idcategoria int,
FOREIGN KEY (idcategoria) REFERENCES categorias(idcategoria),
);

